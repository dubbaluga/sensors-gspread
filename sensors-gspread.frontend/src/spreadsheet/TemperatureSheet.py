'''
Created on Apr 18, 2015

@author: rpoisel
'''

import datetime
import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials


class TemperatureSheet(object):

    LASTROW_ROW = 2
    LASTROW_COL = 11
    COL_DATE = 1
    COL_TEMP_AIR = 2
    COL_REL_HUMIDITY = 3
    COL_TEMP_GROUND = 8

    def __init__(self, credentialspath, spreadsheet_name):
        super().__init__()

        json_key = json.load(open(credentialspath))
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = SignedJwtAssertionCredentials(
                json_key['client_email'],
                json_key['private_key'].encode('utf-8'),
                scope)
        gc = gspread.authorize(credentials)

        self.worksheet = gc.open(spreadsheet_name).sheet1

    def addMeasurement(self, temp_air, relative_humidity, temp_ground):
        today = '{:%m/%d/%Y %H:%M:%S}'.format(datetime.datetime.now())
        sdd = '=6.1078  * POWER(10,((7.5*INDIRECT("B" & ROW())/(237.3+INDIRECT("B" & ROW())))))'
        dd = '=INDIRECT("C" & ROW())/100 * INDIRECT("D" & ROW())'
        v = '=LOG(INDIRECT("E" & ROW())/6.1078, 10)'
        temp_dew_point = '=237.3 * INDIRECT("F" & ROW()) / (7.5 - INDIRECT("F" & ROW()))'
        self.worksheet.append_row([today, temp_air, relative_humidity, sdd, dd, v, temp_dew_point, temp_ground])