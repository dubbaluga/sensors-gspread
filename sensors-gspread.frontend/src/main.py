'''
Created on Apr 18, 2015

@author: rpoisel
'''

import sys

from spreadsheet.TemperatureSheet import TemperatureSheet
from hardware.onewire import OneWireBus
from dht_sensors import AM2302

def main():
    sheet = TemperatureSheet(
                             credentialspath='/root/dev/sensors/frontend/data/sensors-gspread-54c531fcf355.json',
                             spreadsheet_name='Wetterstation Keller')
    onewiresensors = OneWireBus().getSensors()
    if len(onewiresensors) != 1:
        return 1
    
    humidity_sensor = AM2302()
    humidity = humidity_sensor.read_retry(27)
    
    if humidity[0] is None or humidity[1] is None:
        return 1
    
    sheet.addMeasurement(
                         temp_air=humidity[1], relative_humidity=humidity[0],
                         temp_ground=onewiresensors[0].read_temp()
                         )
    return 0

if __name__ == '__main__':
    sys.exit(main())