'''
Created on Apr 16, 2015

@author: rpoisel
'''

import glob
import os.path
import time


class OneWireBus(object):
    
    BASE_DIR = '/sys/bus/w1/devices'
    
    def __init__(self):
        super().__init__()
        self.devices = []

        device_folders = glob.glob(os.path.join(OneWireBus.BASE_DIR, DS18S20.FOLDER_PREFIX) + '*')
        for device_folder in device_folders:
            ds18s20 = DS18S20(device_folder)
            self.devices.append(ds18s20)
    
    def getSensors(self):
        return self.devices


class DS18S20(object):

    FOLDER_PREFIX = '28'
    FILE_SUFFIX = 'w1_slave'
    
    def __init__(self, folder):
        super().__init__()
        self.folder = folder
        self.id = self.folder[self.folder.rfind('-') + 1:]
        
    def __read_temp_raw(self):
        with open(os.path.join(self.folder, DS18S20.FILE_SUFFIX), 'r') as fh:
            return fh.readlines()
    
    def get_id(self):
        return self.id

    def read_temp(self):
        lines = None
        while True:
            lines = self.__read_temp_raw()
            if lines[0].strip()[-3:] == 'YES':
                break
            time.sleep(0.2)

        temp_pos = lines[1].find('t=')
        if temp_pos != -1:
            temp_string = lines[1][temp_pos + 2 :]
            return float(temp_string) / 1000.0
        return -1  # error case