# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import time

import Raspberry_Pi_Driver


class DHT_Sensor(object):

	# Define error constants.
	DHT_SUCCESS        =  0
	DHT_ERROR_TIMEOUT  = -1
	DHT_ERROR_CHECKSUM = -2
	DHT_ERROR_ARGUMENT = -3
	DHT_ERROR_GPIO     = -4
	TRANSIENT_ERRORS = [DHT_ERROR_CHECKSUM, DHT_ERROR_TIMEOUT]
	
	def __init__(self, driver=Raspberry_Pi_Driver):
		super().__init__()
		self.driver = driver

	def read(self, pin):
		# Validate pin is a valid GPIO.
		if pin is None or int(pin) < 0 or int(pin) > 31:
			raise ValueError('Pin must be a valid GPIO number 0 to 31.')
		# Get a reading from C driver code.
		result, humidity, temp = self.driver.read(self.getSensorType(), int(pin))
		if result in DHT_Sensor.TRANSIENT_ERRORS:
			# Signal no result could be obtained, but the caller can retry.
			return (None, None)
		elif result == DHT_Sensor.DHT_ERROR_GPIO:
			raise RuntimeError('Error accessing GPIO. Make sure program is run as root with sudo!')
		elif result != DHT_Sensor.DHT_SUCCESS:
			# Some kind of error occured.
			raise RuntimeError('Error calling DHT test driver read: {0}'.format(result))
		return (humidity, temp)

	def read_retry(self, pin, retries=15, delay_seconds=2):
		"""Read DHT sensor of specified sensor type (DHT11, DHT22, or AM2302) on 
			specified pin and return a tuple of humidity (as a floating point value
			in percent) and temperature (as a floating point value in Celsius).
			Unlike the read function, this read_retry function will attempt to read
			multiple times (up to the specified max retries) until a good reading can be 
			found. If a good reading cannot be found after the amount of retries, a tuple
			of (None, None) is returned. The delay between retries is by default 2
			seconds, but can be overridden.
		"""
		for i in range(retries):
			humidity, temperature = self.read(pin)
			if humidity is not None and temperature is not None:
				return (humidity, temperature)
			time.sleep(delay_seconds)
		return (None, None)


class DHT11(DHT_Sensor):
	
	def getSensorType(self):
		return 11


class DHT22(DHT_Sensor):
	
	def getSensorType(self):
		return 22


class AM2302(DHT_Sensor):
	
	def getSensorType(self):
		return 22
